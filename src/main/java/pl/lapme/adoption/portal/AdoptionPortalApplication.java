package pl.lapme.adoption.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdoptionPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdoptionPortalApplication.class, args);
	}
}
